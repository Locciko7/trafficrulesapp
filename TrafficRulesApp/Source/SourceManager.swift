//
//  SourceManager.swift
//  TrafficRulesApp
//
//  Created by Loci Olah on 21.10.2021.
//

import Foundation

class SourceManager {
    
    func getViolator() -> [ViolatorModel] {
        return setUpViolators()
    }
    
    func deleteViolator(id: String, violators: [ViolatorModel]) -> [ViolatorModel] {
        return violators.filter({$0.id != id})
    }
    
    func addViolator(violator: ViolatorModel, violators: [ViolatorModel]) -> [ViolatorModel] {
        var tempViolator = violators
        tempViolator.append(violator)
        return tempViolator.sorted(by: {$0.firstName < $1.firstName})
    }
    
    func setUpViolators() -> [ViolatorModel] {
        let firstViolator = ViolatorModel(id: "1", firstName: "Petrov", secondName: "Petya", fine: "Перевищення швидкості", amount: 100, isSelected: false)
        let secondViolator = ViolatorModel(id: "2", firstName: "Ivanov", secondName: "Ivan", fine: "Не правильна парковка", amount: 110, isSelected: false)
        let thirdViolator = ViolatorModel(id: "3", firstName: "Grachov", secondName: "Andrey", fine: "Подвійний обгін", amount: 200, isSelected: false)
        let foursViolator = ViolatorModel(id: "4", firstName: "Pop", secondName: "Petya", fine: "Перевищення швидкості", amount: 150, isSelected: false)
        let fifthViolator = ViolatorModel(id: "5", firstName: "Gabor", secondName: "Ishtvan", fine: "Не правильна парковка", amount: 90, isSelected: false)
        let sixViolator = ViolatorModel(id: "6", firstName: "Akulenko", secondName: "Kiril", fine: "Не правильна парковка", amount: 100, isSelected: false)
        let sevenViolator = ViolatorModel(id: "7", firstName: "Shtefan", secondName: "Sergey", fine: "Не правильна парковка", amount: 70, isSelected: false)
        
        return [firstViolator, secondViolator, thirdViolator, foursViolator, fifthViolator, sixViolator, sevenViolator]
    }
}
