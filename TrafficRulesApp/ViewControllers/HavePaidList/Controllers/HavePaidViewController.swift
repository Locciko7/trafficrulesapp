//
//  HavePaidViewController.swift
//  TrafficRulesApp
//
//  Created by Loci Olah on 21.10.2021.
//

import UIKit

class HavePaidViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var havePaid: [ViolatorModel] = []
    let violatorCellID = "ViolatorCell"

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: violatorCellID, bundle: nil), forCellReuseIdentifier: violatorCellID)

    }
    
    override func  viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tableView.reloadData()
    }
    

}
