//
//  HavePaidViewController+Extensions.swift
//  TrafficRulesApp
//
//  Created by Loci Olah on 24.10.2021.
//

import Foundation
import UIKit

extension HavePaidViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return havePaid.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: violatorCellID, for: indexPath) as! ViolatorCell
        cell.update(violator: havePaid[indexPath.row])
        return cell
    }
    
}
