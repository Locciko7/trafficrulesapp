//
//  ViolatorModel.swift
//  TrafficRulesApp
//
//  Created by Loci Olah on 21.10.2021.
//

import Foundation

struct ViolatorModel {
    var id: String
    var firstName: String
    var secondName: String
    var fine: String
    var amount: Int?
    var isSelected: Bool
    
    var amounString: String {
        return amount == nil ? "0" : "\(amount!)"
    }
}
