//
//  ViolatorViewController+Extensions.swift
//  TrafficRulesApp
//
//  Created by Loci Olah on 21.10.2021.
//

import UIKit

// MARK: TableView Delegate, TableView Datasource
extension ViolatorsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return violators.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: violatorCellID, for: indexPath) as! ViolatorCell
        cell.update(violator: violators[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            var havePaidViolators: [ViolatorModel] = []
            
            let deletedVC = (self.tabBarController?.viewControllers?[2] as? HavePaidViewController)
            havePaidViolators = deletedVC?.havePaid ?? []
            havePaidViolators.append(violators[indexPath.row])
            
            
            violators.remove(at: indexPath.row)
            
            tableView.deleteRows(at: [indexPath], with: .fade)
            deletedVC?.havePaid = havePaidViolators
           
        }
    }
    
}
