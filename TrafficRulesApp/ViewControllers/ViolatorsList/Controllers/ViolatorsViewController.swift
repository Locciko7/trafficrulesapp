//
//  ViolatorsViewController.swift
//  TrafficRulesApp
//
//  Created by Loci Olah on 21.10.2021.
//

import UIKit

class ViolatorsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    let violatorCellID = "ViolatorCell"
    var violators: [ViolatorModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: violatorCellID, bundle: nil), forCellReuseIdentifier: violatorCellID)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if violators.isEmpty {
            violators = SourceManager().setUpViolators()
        }
        tableView.reloadData()
    }
}
