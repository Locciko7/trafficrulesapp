//
//  ViolatorCell.swift
//  TrafficRulesApp
//
//  Created by Loci Olah on 21.10.2021.
//

import UIKit

class ViolatorCell: UITableViewCell {

    @IBOutlet weak var violatorFirstNameLabel: UILabel!
    @IBOutlet weak var violatorSecondNameLabel: UILabel!
    @IBOutlet weak var violatorFine: UILabel!
    @IBOutlet weak var violatorAmount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func update(violator: ViolatorModel) {
        
        violatorFirstNameLabel.text = violator.firstName
        violatorSecondNameLabel.text = violator.secondName
        violatorFine.text = violator.fine
        violatorAmount.text = violator.amounString
    }
    
}
