//
//  DeletedViewController.swift
//  TrafficRulesApp
//
//  Created by Loci Olah on 21.10.2021.
//

import UIKit

class NewViolatorViewController: UIViewController {
    
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var secondNameTextField: UITextField!
    @IBOutlet weak var fineTextField: UITextField!
    @IBOutlet weak var amoutFineTextField: UITextField!
    
    var violators: [ViolatorModel] = []

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func addViolator(_ sender: Any) {
        addProduct()
    }
    
    func addProduct() {
        guard let firstName = firstNameTextField.text, let secondName = secondNameTextField.text, let fine = fineTextField.text, let amountFine = amoutFineTextField.text, !firstName.isEmpty, !secondName.isEmpty, !fine.isEmpty, !amountFine.isEmpty else {
            return
        }
        
        let addedViolator = ViolatorModel(id: "100", firstName: firstName, secondName: secondName, fine: fine, amount: Int(amoutFineTextField.text ?? "0"), isSelected: false)
        
        let violatorsVC = (self.tabBarController?.viewControllers?[0] as? ViolatorsViewController)
        self.violators = violatorsVC?.violators ?? []
        self.violators = SourceManager().addViolator(violator: addedViolator, violators: violators)
        violatorsVC?.violators = self.violators
        self.tabBarController?.selectedIndex = 0
    }
    
    func showErrorAlert() {
        let alertVC = UIAlertController(title: "Error", message: "Please, enter all fields!", preferredStyle: .alert)
        let  cancelAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alertVC.addAction(cancelAction)
        present(alertVC, animated: true, completion: nil)
    }

}
